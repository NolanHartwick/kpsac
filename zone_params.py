# crop dimensions, upper left x, y, width, height
sector_crop_list = [
    [50, 50, 250, 250],  # sector 1
    [0, 0, 250, 250],  # sector 2
    [50, 250, 250, 250],  # sector 3
    [250, 0, 250, 250],  # sector 4
    [150, 150, 250, 250],  # sector 5/17
    [200, 100, 250, 250],  # sector 6
    [200, 150, 250, 250],  # sector 7
    [250, 50, 250, 250],  # sector 8
    [250, 150, 250, 250],  # sector 9
    [300, 200, 250, 250],  # sector 10
    [400, 100, 250, 250],  # sector 11
    [350, 200, 250, 250],  # sector 12
    [410, 0, 250, 250],  # sector 13
    [410, 200, 250, 250],  # sector 14
    [410, 0, 250, 250],  # sector 15
    [410, 200, 250, 250]  # sector 16
]

# a list of lists (one per zone) of tuples stating where to mask to
# y, x, yw, xw
# where y=512 and x=660
zone_slice_list = [
    [   # zone 1
        [0, 150, 210, 250],
        [0, 150, 220, 240],
        [0, 160, 230, 230],
        None,
        None,
        None,
        [320, 130, 512, 240],
        [300, 130, 512, 260],
        [300, 140, 512, 250],
        [300, 150, 512, 240],
        [300, 130, 512, 240],
        None,
        None,
        [0, 160, 250, 230],
        [0, 150, 200, 230],
        [0, 150, 200, 250]
    ],
    [   # zone 2
        [0, 0, 230, 180],
        [0, 0, 230, 180],
        [0, 0, 230, 180],
        None,
        None,
        None,
        [300, 0, 512, 180],
        [300, 0, 512, 180],
        [300, 0, 512, 180],
        [300, 0, 512, 180],
        [300, 0, 512, 180],
        None,
        None,
        [0, 0, 230, 180],
        [0, 0, 230, 180],
        [0, 0, 230, 180]
    ],
    [   # zone 3
        [300, 140, 512, 250],
        [300, 140, 512, 250],
        [300, 140, 512, 250],
        [300, 140, 512, 250],
        None,
        None,
        [0, 150, 220, 240],
        [0, 150, 220, 240],
        [0, 150, 220, 240],
        [0, 150, 220, 240],
        [0, 150, 220, 240],
        [0, 150, 220, 240],
        None,
        None,
        [300, 140, 512, 250],
        [300, 140, 512, 250]
    ],
    [   # zone 4
        [300, 0, 512, 180],
        [300, 0, 512, 180],
        [300, 0, 512, 180],
        [300, 0, 512, 180],
        None,
        None,
        [0, 0, 230, 180],
        [0, 0, 230, 180],
        [0, 0, 230, 180],
        [0, 0, 230, 180],
        [0, 0, 230, 180],
        [0, 0, 230, 180],
        None,
        None,
        [300, 0, 512, 180],
        [300, 0, 512, 180]
    ],
    [   # zone 5
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None
    ],
    [   # zone 6
        [0, 290, 280, 370],
        [0, 290, 280, 370],
        None,
        None,
        None,
        None,
        None,
        None,
        [230, 290, 512, 370],
        [230, 290, 512, 370],
        [230, 290, 512, 370],
        [0, 290, 280, 370],
        [0, 290, 280, 370],
        [0, 290, 280, 370],
        [0, 290, 280, 370],
        [0, 290, 280, 370]
    ],
    [   # zone 7
        [230, 290, 512, 370],
        [230, 290, 512, 370],
        [230, 290, 512, 370],
        [230, 290, 512, 370],
        [230, 290, 512, 370],
        [230, 290, 512, 370],
        [0, 290, 280, 370],
        [0, 290, 280, 370],
        [0, 290, 280, 370],
        [0, 290, 280, 370],
        None,
        None,
        None,
        None,
        None,
        None
    ],
    [   # zone 8
        [0, 360, 240, 460],
        [0, 360, 240, 460],
        None,
        None,
        None,
        None,
        None,
        [240, 360, 512, 460],
        [240, 360, 512, 460],
        [240, 360, 512, 460],
        [240, 360, 512, 460],
        [240, 360, 512, 460],
        [0, 360, 260, 460],
        [0, 360, 260, 460],
        [0, 360, 260, 460],
        [0, 360, 260, 460]
    ],
    [   # zone 9
        [215, 360, 275, 460],
        [215, 360, 275, 460],
        [0, 360, 245, 460],
        [0, 360, 245, 460],
        [0, 360, 245, 460],
        None,
        None,
        None,
        [215, 360, 275, 460],
        [215, 360, 275, 460],
        None,
        None,
        None,
        None,
        [215, 360, 275, 460],
        [215, 360, 275, 460],
    ],
    [   # zone 10
        [240, 360, 512, 460],
        [240, 360, 512, 460],
        [240, 360, 512, 460],
        [240, 360, 512, 460],
        [240, 360, 512, 460],
        [0, 360, 260, 460],
        [0, 360, 260, 460],
        [0, 360, 260, 460],
        [0, 360, 260, 460],
        None,
        None,
        None,
        None,
        None,
        [240, 360, 512, 460],
        [240, 360, 512, 460],
    ],
    [   # zone 11
        [0, 450, 256, 525],
        [0, 450, 256, 525],
        [0, 450, 256, 525],
        [0, 450, 256, 525],
        None,
        None,
        [256, 450, 512, 525],
        [256, 450, 512, 525],
        [256, 450, 512, 525],
        [256, 450, 512, 525],
        [256, 450, 512, 525],
        None,
        [0, 450, 256, 525],
        [0, 450, 256, 525],
        [0, 450, 256, 525],
        [0, 450, 256, 525]
    ],
    [   # zone 12
        [256, 450, 512, 525],
        [256, 450, 512, 525],
        [256, 450, 512, 525],
        [256, 450, 512, 525],
        [256, 450, 512, 525],
        [0, 450, 256, 525],
        [0, 450, 256, 525],
        [0, 450, 256, 525],
        [0, 450, 256, 525],
        [0, 450, 256, 525],
        None,
        None,
        None,
        [256, 450, 512, 525],
        [256, 450, 512, 525],
        [256, 450, 512, 525]
    ],
    [   # zone 13
        [0, 500, 256, 600],
        [0, 500, 256, 600],
        [0, 500, 256, 600],
        [0, 500, 256, 600],
        None,
        None,
        [256, 500, 512, 600],
        [256, 500, 512, 600],
        [256, 500, 512, 600],
        [256, 500, 512, 600],
        [256, 500, 512, 600],
        None,
        [0, 500, 256, 600],
        [0, 500, 256, 600],
        [0, 500, 256, 600],
        [0, 500, 256, 600]
    ],
    [   # zone 14
        [256, 500, 512, 600],
        [256, 500, 512, 600],
        [256, 500, 512, 600],
        [256, 500, 512, 600],
        [256, 500, 512, 600],
        None,
        [0, 500, 256, 600],
        [0, 500, 256, 600],
        [0, 500, 256, 600],
        [0, 500, 256, 600],
        [0, 500, 256, 600],
        None,
        None,
        [256, 500, 512, 600],
        [256, 500, 512, 600],
        [256, 500, 512, 600]
    ],
    [   # zone 15
        [0, 570, 256, 660],
        [0, 570, 256, 660],
        [0, 570, 256, 660],
        [0, 570, 256, 660],
        None,
        None,
        [256, 570, 512, 660],
        [256, 570, 512, 660],
        [256, 570, 512, 660],
        [256, 570, 512, 660],
        None,
        [0, 570, 256, 660],
        [0, 570, 256, 660],
        [0, 570, 256, 660],
        [0, 570, 256, 660],
        [0, 570, 256, 660],
    ],
    [   # zone 16
        [256, 570, 512, 660],
        [256, 570, 512, 660],
        [256, 570, 512, 660],
        [256, 570, 512, 660],
        [256, 570, 512, 660],
        [256, 570, 512, 660],
        [0, 570, 256, 660],
        [0, 570, 256, 660],
        [0, 570, 256, 660],
        [0, 570, 256, 660],
        [0, 570, 256, 660],
        None,
        None,
        None,
        [256, 570, 512, 660],
        [256, 570, 512, 660]
    ],
    [   # zone 17
        None,
        None,
        None,
        None,
        None,
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        [0, 220, 512, 300],
        None,
        None,
        None
    ]
]

# a list of lists (one per zone) of tuples stating where cropping should occur
# y, x, yw, xw
# where y=660 and x=512
zone_crop_list = [
    [  # threat zone 1
        sector_crop_list[0], sector_crop_list[0], sector_crop_list[0], None,
        None, None, sector_crop_list[2], sector_crop_list[2],
        sector_crop_list[2], sector_crop_list[2], sector_crop_list[2], None,
        None, sector_crop_list[0], sector_crop_list[0],
        sector_crop_list[0]
    ],
    [  # threat zone 2
        sector_crop_list[1], sector_crop_list[1], sector_crop_list[1], None,
        None, None, sector_crop_list[2], sector_crop_list[2],
        sector_crop_list[2], sector_crop_list[2], sector_crop_list[2],
        None, None, sector_crop_list[1], sector_crop_list[1],
        sector_crop_list[1]
    ],
    [  # threat zone 3
        sector_crop_list[2], sector_crop_list[2], sector_crop_list[2],
        sector_crop_list[2], None, None, sector_crop_list[0],
        sector_crop_list[0], sector_crop_list[0], sector_crop_list[0],
        sector_crop_list[0], sector_crop_list[0], None, None,
        sector_crop_list[2], sector_crop_list[2]
    ],
    [  # threat zone 4
        sector_crop_list[2], sector_crop_list[2], sector_crop_list[2],
        sector_crop_list[2], None, None, sector_crop_list[1],
        sector_crop_list[1], sector_crop_list[1], sector_crop_list[1],
        sector_crop_list[1], sector_crop_list[1], None, None,
        sector_crop_list[2], sector_crop_list[2]
    ],
    [  # threat zone 5
        sector_crop_list[4], sector_crop_list[4], sector_crop_list[4],
        sector_crop_list[4], sector_crop_list[4], sector_crop_list[4],
        sector_crop_list[4], sector_crop_list[4],
        None, None, None, None, None, None, None, None
    ],
    [  # threat zone 6
        sector_crop_list[5], sector_crop_list[5], None, None, None,
        None, None, None, sector_crop_list[6], sector_crop_list[6],
        sector_crop_list[6], sector_crop_list[5], sector_crop_list[5],
        sector_crop_list[5], sector_crop_list[5], sector_crop_list[5]
    ],
    [  # threat zone 7
        sector_crop_list[6], sector_crop_list[6], sector_crop_list[6],
        sector_crop_list[6], sector_crop_list[6], sector_crop_list[6],
        sector_crop_list[5], sector_crop_list[5], sector_crop_list[5],
        sector_crop_list[5], None, None, None, None, None, None
    ],
    [  # threat zone 8
        sector_crop_list[7], sector_crop_list[7], None, None, None,
        None, None, sector_crop_list[9], sector_crop_list[9],
        sector_crop_list[9], sector_crop_list[9], sector_crop_list[9],
        sector_crop_list[7], sector_crop_list[7], sector_crop_list[7],
        sector_crop_list[7]
    ],
    [  # threat zone 9
        sector_crop_list[8], sector_crop_list[8], sector_crop_list[7],
        sector_crop_list[7], sector_crop_list[7], None, None, None,
        sector_crop_list[8], sector_crop_list[8], None, None, None,
        None, sector_crop_list[9], sector_crop_list[8]
    ],
    [  # threat zone 10
        sector_crop_list[9], sector_crop_list[9], sector_crop_list[9],
        sector_crop_list[9], sector_crop_list[9], sector_crop_list[7],
        sector_crop_list[7], sector_crop_list[7], sector_crop_list[7],
        None, None, None, None, None, sector_crop_list[9],
        sector_crop_list[9]
    ],
    [  # threat zone 11
        sector_crop_list[10], sector_crop_list[10], sector_crop_list[10],
        sector_crop_list[10], None, None, sector_crop_list[11],
        sector_crop_list[11], sector_crop_list[11], sector_crop_list[11],
        sector_crop_list[11], None, sector_crop_list[10],
        sector_crop_list[10], sector_crop_list[10], sector_crop_list[10]
    ],
    [  # threat zone 12
        sector_crop_list[11], sector_crop_list[11], sector_crop_list[11],
        sector_crop_list[11], sector_crop_list[10], sector_crop_list[10],
        sector_crop_list[10], sector_crop_list[10], sector_crop_list[10],
        sector_crop_list[10], None, None, None,
        sector_crop_list[11], sector_crop_list[11], sector_crop_list[11]
    ],
    [  # threat zone 13
        sector_crop_list[12], sector_crop_list[12], sector_crop_list[12],
        sector_crop_list[12], None, None, sector_crop_list[13],
        sector_crop_list[13], sector_crop_list[13], sector_crop_list[13],
        sector_crop_list[13], None, sector_crop_list[12],
        sector_crop_list[12], sector_crop_list[12], sector_crop_list[12]
    ],
    [  # sector 14
        sector_crop_list[13], sector_crop_list[13], sector_crop_list[13],
        sector_crop_list[13], sector_crop_list[13], None,
        sector_crop_list[12], sector_crop_list[12], sector_crop_list[12],
        sector_crop_list[12], sector_crop_list[12], None, None,
        sector_crop_list[13], sector_crop_list[13], sector_crop_list[13],
    ],
    [  # threat zone 15
        sector_crop_list[14], sector_crop_list[14], sector_crop_list[14],
        sector_crop_list[14], None, None, sector_crop_list[15],
        sector_crop_list[15], sector_crop_list[15], sector_crop_list[15],
        None, sector_crop_list[14], sector_crop_list[14],
        sector_crop_list[14], sector_crop_list[14], sector_crop_list[14]
    ],
    [  # threat zone 16
        sector_crop_list[15], sector_crop_list[15], sector_crop_list[15],
        sector_crop_list[15], sector_crop_list[15], sector_crop_list[15],
        sector_crop_list[14], sector_crop_list[14], sector_crop_list[14],
        sector_crop_list[14], sector_crop_list[14], None, None, None,
        sector_crop_list[15], sector_crop_list[15]
    ],
    [  # threat zone 17
        None, None, None, None, None,
        sector_crop_list[4], sector_crop_list[4], sector_crop_list[4],
        sector_crop_list[4], sector_crop_list[4], sector_crop_list[4],
        sector_crop_list[4], sector_crop_list[4], None, None, None,
    ]
]
