from keras.models import load_model, Model
from keras.layers import (
    Dense,
    Dropout,
    Flatten,
    Activation,
    Conv3D,
    MaxPooling3D,
    normalization,
    Input,
)
from keras.callbacks import EarlyStopping, CSVLogger, ModelCheckpoint
from keras.preprocessing.image import ImageDataGenerator
from keras.losses import binary_crossentropy
from keras.optimizers import Adadelta
from keras import backend as tf

from hyperopt import tpe, fmin, hp

from random import shuffle, seed
import numpy as np
import os
import argparse

import misc

initial_epoch = [0]
print('loading_data')
image_paths = misc.get_image_paths()
image_keys = [k for k in image_paths]
image_stack = np.array([misc.get_image(image_paths[k]) for k in image_keys])
image_keys = {k: i for i, k in enumerate(image_keys)}
model_path_template = '../models/zone_{0!s}/'
csv_path_template = '../models/zone_{0!s}/log_zone_{0!s}.csv'
model_name_template = 'weights.{epoch}.hdf5'
print('data_loaded')
print('\n\n\n')


def data(zone):
    """ return 4 numpy arrays representing the data
        (x_train, y_train), (x_test, y_test)
    """
    train_size = 0.75
    seed_val = 898989
    labels = misc.load_labels()
    # labels = {k: labels[k] for k in labels if(sum(labels[k]) <= 1)}
    plus_keys = [l for l in labels if(labels[l][zone] == 1)]
    neg_keys = [l for l in labels if(labels[l][zone] == 0)]

    seed(seed_val)
    shuffle(plus_keys)
    shuffle(neg_keys)

    plus_train_idx = int(len(plus_keys) * train_size)
    neg_train_idx = int(len(neg_keys) * train_size)

    train_keys = plus_keys[:plus_train_idx] + neg_keys[:neg_train_idx]
    valid_keys = plus_keys[plus_train_idx:] + neg_keys[neg_train_idx:]

    def _helper(keys):
        y1 = np.array([labels[k][zone] for k in keys])
        # y2 = np.array([sum(labels[k]) > 0 for k in keys])
        idxes = [image_keys[k] for k in keys]
        x = image_stack[idxes, :, :, :]
        return x, y1

    return _helper(train_keys), _helper(valid_keys)


def augmenter(itr, zone):
    """ apply image augmentation and then crop down to zone
    """
    while(True):
        x, y = next(itr)
        x = misc.crop_images(x, zone)
        yield (x, y)


def conv_block(i, filters, pool):
    """ Simple conv block with some parameters allowing control
        of pooling (boolean) and number of filters (int)
    """
    x = Conv3D(filters, kernel_size=(3, 3, 1))(i)
    x = normalization.BatchNormalization()(x)
    x = Activation('elu')(x)
    if(pool):
        x = MaxPooling3D(pool_size=(2, 2, 1))(x)
    return x


def create_model(params):
    """ Creates and trains a model defined by params
    """
    # read params and ready data
    wsr = params['wsr']
    hsr = params['hsr']
    rr = params['rr']
    zone = params['zone']
    batch_size = 32
    epochs = 2**8
    (x_tn, y_tn), (x_v, y_v) = data(zone)
    x_v = misc.crop_images(x_v, zone)

    # Define the model
    input_zone = Input(shape=x_v.shape[1:])
    x = conv_block(input_zone, 4, True)
    x = conv_block(x, 6, True)
    x = conv_block(x, 8, True)
    x = conv_block(x, 10, False)
    x = Flatten()(x)
    # x = Dropout(0.5)(x)
    # x = Dense(100)(x)
    x = Dropout(0.5)(x)
    x = Dense(1)(x)
    x = Activation('sigmoid', name='zone')(x)
    model = Model(inputs=input_zone, outputs=x)
    model.compile(
        loss=binary_crossentropy,
        optimizer=Adadelta()
    )

    # define the data augmentation parameters
    # (these are the params being hyper optimized)
    itr = ImageDataGenerator(
        height_shift_range=hsr,
        width_shift_range=wsr,
        rotation_range=rr,
    ).flow(x_tn, y_tn, batch_size=batch_size)

    # Create a directory to store log files and model snapshots
    path = model_path_template.format(zone)
    if(not os.path.exists(path)):
        os.makedirs(path)
    model_path = os.path.join(path, model_name_template)

    # define stopping criteria, checkpointer, and logger
    checkpointer = ModelCheckpoint(filepath=model_path, verbose=0, save_best_only=True)
    csv = CSVLogger(csv_path_template.format(zone), append=True)
    stopper = EarlyStopping(patience=12, min_delta=10**-4)

    # Train the model, update current epoch and return results
    train_hist = model.fit_generator(
        augmenter(itr, zone),
        steps_per_epoch=len(x_tn) / batch_size,
        epochs=epochs + initial_epoch[0],
        validation_data=(x_v, y_v),
        verbose=1,
        callbacks=[stopper, checkpointer, csv],
        initial_epoch=initial_epoch[0]
    )
    initial_epoch[0] += len(train_hist.history['val_loss'])
    tf.clear_session()
    return min(train_hist.history['val_loss'])


def train(zones):
    """ Function impliments some hyper parameter optimizations and
        executes model training.
    """
    for zone in zones:
        initial_epoch[0] = 0
        space = {
            'rr': hp.uniform('rr', 0, 50),
            'wsr': hp.uniform('wsr', 0, 0.1),
            'hsr': hp.uniform('hsr', 0, 0.1),
            'zone': hp.choice('zone', [zone])
        }
        best_run = fmin(
            create_model,
            space,
            algo=tpe.suggest,
            max_evals=60
        )
        print('\n\nZone {0!s}'.format(zone))
        print("Best performing model chosen hyper-parameters:")
        print(best_run)


def get_best_model(zone):
    """ Function parses through the relevant log file for zone,
        identifies the best model, loads and returns it
    """
    csv = csv_path_template.format(zone)
    with open(csv) as temp_f:
        log_data = temp_f.read().split()[1:]
    log_data = [l.split(',') for l in log_data]
    best_entry = min(log_data, key=lambda x: float(x[-1]))
    best_epoch = int(best_entry[0]) + 1
    model_name = model_name_template.format(epoch=best_epoch)
    model_path = os.path.abspath(model_path_template.format(zone))
    model_path = os.path.join(model_path, model_name)
    model = load_model(model_path)
    return model


def gen_submission():
    """ Using the best model for each zone, generate a submission file which
        contains test image predictions
    """
    labels = misc.load_labels()
    test_keys = [l for l in image_keys if(l not in labels)]
    idxes = [image_keys[k] for k in test_keys]
    test_images = image_stack[idxes, :, :, :]
    predictions = {k: list(range(17)) for k in test_keys}
    for zone in range(17):
        model = get_best_model(zone)
        x_ts = misc.crop_images(test_images, zone)
        results = model.predict(x_ts)
        for k, p in zip(predictions, results):
            predictions[k][zone] = p[0]
    out_path = 'submission.csv'
    with open(out_path, 'w') as f:
        f.write('Id,Probability\n')
        for k in predictions:
            for i, p in enumerate(predictions[k]):
                f.write('{0!s}_Zone{1!s},{2!s}\n'.format(k, i + 1, p))


if(__name__ == '__main__'):
    parser = argparse.ArgumentParser()
    parser.add_argument('zone', nargs='*', type=int)
    args = parser.parse_args()
    train(args.zone)
    # gen_submission()
