import numpy as np
import os
from matplotlib import pyplot
from skimage.transform import resize

import zone_params

""" This module defines some functions used for loading data as well as
    impliments some functions for basic analysis of the data
"""

# Define relevant variables for finding data and labels
data_dir = os.path.abspath('../data')
stage1 = os.path.join(data_dir, 'stage1_aps')
# stage1_labels_corrected swaps the labels for
# 623c761b4db398ea2157e6c5cd6c8c58 and 496ec724cc1f2886aac5840cf890988a
label_path = os.path.join(data_dir, 'stage1_labels_corrected.csv')
# this image is incorrectly encoded/corrupted and should be ignored
ignore_keys = set([
    '42181583618ce4bbfbc0c4c300108bf5',
])


def read_header(infile):
    """ Read image header (first 512 bytes)

        NOTE : This function came from the discussion for this competition.
        I didn't write this function
    """
    h = dict()
    fid = open(infile, 'r+b')
    h['filename'] = b''.join(np.fromfile(fid, dtype='S1', count=20))
    h['parent_filename'] = b''.join(np.fromfile(fid, dtype='S1', count=20))
    h['comments1'] = b''.join(np.fromfile(fid, dtype='S1', count=80))
    h['comments2'] = b''.join(np.fromfile(fid, dtype='S1', count=80))
    h['energy_type'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['config_type'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['file_type'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['trans_type'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['scan_type'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['data_type'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['date_modified'] = b''.join(np.fromfile(fid, dtype='S1', count=16))
    h['frequency'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['mat_velocity'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['num_pts'] = np.fromfile(fid, dtype=np.int32, count=1)
    h['num_polarization_channels'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['spare00'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['adc_min_voltage'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['adc_max_voltage'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['band_width'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['spare01'] = np.fromfile(fid, dtype=np.int16, count=5)
    h['polarization_type'] = np.fromfile(fid, dtype=np.int16, count=4)
    h['record_header_size'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['word_type'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['word_precision'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['min_data_value'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['max_data_value'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['avg_data_value'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['data_scale_factor'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['data_units'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['surf_removal'] = np.fromfile(fid, dtype=np.uint16, count=1)
    h['edge_weighting'] = np.fromfile(fid, dtype=np.uint16, count=1)
    h['x_units'] = np.fromfile(fid, dtype=np.uint16, count=1)
    h['y_units'] = np.fromfile(fid, dtype=np.uint16, count=1)
    h['z_units'] = np.fromfile(fid, dtype=np.uint16, count=1)
    h['t_units'] = np.fromfile(fid, dtype=np.uint16, count=1)
    h['spare02'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['x_return_speed'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['y_return_speed'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['z_return_speed'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['scan_orientation'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['scan_direction'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['data_storage_order'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['scanner_type'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['x_inc'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['y_inc'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['z_inc'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['t_inc'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['num_x_pts'] = np.fromfile(fid, dtype=np.int32, count=1)
    h['num_y_pts'] = np.fromfile(fid, dtype=np.int32, count=1)
    h['num_z_pts'] = np.fromfile(fid, dtype=np.int32, count=1)
    h['num_t_pts'] = np.fromfile(fid, dtype=np.int32, count=1)
    h['x_speed'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['y_speed'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['z_speed'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['x_acc'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['y_acc'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['z_acc'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['x_motor_res'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['y_motor_res'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['z_motor_res'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['x_encoder_res'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['y_encoder_res'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['z_encoder_res'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['date_processed'] = b''.join(np.fromfile(fid, dtype='S1', count=8))
    h['time_processed'] = b''.join(np.fromfile(fid, dtype='S1', count=8))
    h['depth_recon'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['x_max_travel'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['y_max_travel'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['elevation_offset_angle'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['roll_offset_angle'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['z_max_travel'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['azimuth_offset_angle'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['adc_type'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['spare06'] = np.fromfile(fid, dtype=np.int16, count=1)
    h['scanner_radius'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['x_offset'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['y_offset'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['z_offset'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['t_delay'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['range_gate_start'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['range_gate_end'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['ahis_software_version'] = np.fromfile(fid, dtype=np.float32, count=1)
    h['spare_end'] = np.fromfile(fid, dtype=np.float32, count=10)
    return h


def read_data(infile):
    """ Read any of the 4 types of image files, returns a numpy array of the
        image contents

        NOTE : This function came from the discussion for this competition.
        I didn't write this function
    """
    extension = os.path.splitext(infile)[1]
    h = read_header(infile)
    nx = int(h['num_x_pts'])
    ny = int(h['num_y_pts'])
    nt = int(h['num_t_pts'])
    fid = open(infile, 'rb')
    fid.seek(512)  # skip header
    if extension == '.aps' or extension == '.a3daps':
        if(h['word_type'] == 7):  # float32
            data = np.fromfile(fid, dtype=np.float32, count=nx * ny * nt)
        elif(h['word_type'] == 4):  # uint16
            data = np.fromfile(fid, dtype=np.uint16, count=nx * ny * nt)
        data = data * h['data_scale_factor']  # scaling factor
        data = data.reshape(nx, ny, nt, order='F').copy()  # make N-d image
    elif extension == '.a3d':
        if(h['word_type'] == 7):  # float32
            data = np.fromfile(fid, dtype=np.float32, count=nx * ny * nt)
        elif(h['word_type'] == 4):  # uint16
            data = np.fromfile(fid, dtype=np.uint16, count=nx * ny * nt)
        data = data * h['data_scale_factor']  # scaling factor
        data = data.reshape(nx, nt, ny, order='F').copy()  # make N-d image
    elif extension == '.ahi':
        data = np.fromfile(fid, dtype=np.float32, count=2 * nx * ny * nt)
        data = data.reshape(2, ny, nx, nt, order='F').copy()
        real = data[0, :, :, :].copy()
        imag = data[1, :, :, :].copy()
    fid.close()
    if extension != '.ahi':
        return data
    else:
        return real, imag


def load_labels(path=label_path):
    """ read the stage1_labels. and return a dict representing label data
        returns dict[key]->list()
    """
    f = open(path)
    data = f.readlines()[1:]
    data = [e.split(',') for e in data]
    ret = {}
    for e, l in data:
        key, zone = e.split('_')
        # subtract 1 from zone id to make zone ids 0 indexed
        zone = int(zone[4:]) - 1
        if(key not in ret):
            ret[key] = [0 for i in range(17)]
        ret[key][zone] = float(l)
    ret = {k: ret[k] for k in ret if(k not in ignore_keys)}
    return ret


def get_image_paths(path=stage1):
    """ Read the stage1 data directory and return a dict mapping image
        keys to the path to the image
        returns dict[key]->str
    """
    files = os.listdir(path)
    ret = {f.split('.')[0]: os.path.join(path, f) for f in files}
    ret = {k: ret[k] for k in ret if(k not in ignore_keys)}
    return ret


def get_image(path):
    """ load image located at path. Resize the image to 128x165, standardize
        the image
    """
    ret = resize(read_data(path), (128, 165))
    ret = (ret - np.mean(ret)) / np.std(ret)
    ret = ret.transpose([1, 0, 2])
    ret = np.flipud(ret)
    return ret


def label_stats():
    """ compute and print some basic stats for the labels
    """
    labels = load_labels()
    print('total labels : {0!s}'.format(len(labels)))
    for z in range(17):
        print(z)
        val = sum(labels[l][z] for l in labels) / float(len(labels))
        print(val)
    ret = {}
    for l in labels:
        val = sum(labels[l])
        if(val not in ret):
            ret[val] = 0
        ret[val] += 1
    print(ret)


def draw_pcolor(data3, title='', dir_path='', save=True):
    """ Draw a series of images representing data3. Save them if desired
        using dir_path and title
    """
    dir_path = os.path.abspath(dir_path)
    ret = []
    for i in range(data3.shape[-1]):
        f = pyplot.figure()
        pyplot.pcolormesh(np.flipud(data3[:, :, i].transpose()))
        pyplot.axis('tight')
        pyplot.colorbar()
        temp_title = title + '_' + str(i)
        pyplot.title(temp_title)
        if(save):
            if(not os.path.exists(dir_path)):
                os.makedirs(dir_path)
            path = os.path.join(os.path.abspath(dir_path), temp_title)
            pyplot.savefig(path + '.jpg')
        ret.append(f)
    return ret


def pixel_wise_mean_and_std(keys=None, norm_func=lambda x: x):
    """ compute pixel wise means and standard deviations of the images and
        return the resultant data.
        Input:
            keys : [str]
                optional list of image keys to compute metrics for. If not
                present, use all images
            norm_func : callable()
                a function to apply to the images prior to computing metrics.
                Defaults to identity function, lambda x: x.
        Returns : (np.array, np.array)
            A tuple of numpy arrays representing pixel_wise means and standard
            deviations respectively.
    """
    paths = get_image_paths()
    if(keys is None):
        keys = list(key for key in paths)
    mean_data = sum(norm_func(read_data(paths[key])) for key in keys) / len(keys)
    var_data = sum(norm_func(read_data(paths[key]) - mean_data) ** 2 for key in keys)
    var_data /= len(keys) - 1
    return mean_data, var_data


def gen_plots():
    """ Generate and save plots representing pixel_wise_mean_and_std metrics
    """
    mean_data, var_data = pixel_wise_mean_and_std()
    draw_pcolor(mean_data, title='mean_image__layer', dir_path='clean_plots/mean')
    pyplot.close('all')
    draw_pcolor(var_data, title='var_image__layer', dir_path='clean_plots/std')
    pyplot.close('all')


def generate_sample_images():
    """ draw images which ensures at least one positive and negative example
        of each body zone is represented
    """
    labels = load_labels()
    rep_sample_plus = {zone: 0 for zone in range(17)}
    rep_sample_minus = {zone: 0 for zone in range(17)}
    keys = set()
    for key in labels:
        for zone in range(17):
            if(labels[key][zone] == 1 and rep_sample_plus[zone] == 0):
                keys.add(key)
                rep_sample_plus[zone] = 1
            if(labels[key][zone] == 0 and rep_sample_minus[zone] == 0):
                keys.add(key)
                rep_sample_minus[zone] = 1
        if(sum(rep_sample_minus.values()) == 17):
            if(sum(rep_sample_plus.values()) == 17):
                break

    image_paths = get_image_paths()

    if(not os.path.exists('clean_plots/images')):
        os.mkdirs('clean_plots/images')

    print(len(keys))

    for k in keys:
        path = 'clean_plots/images/image_set_{0!s}'.format(k)
        if(not os.path.exists(path)):
            os.mkdir(path)
        print(k)
        print(labels[k])
        data = read_data(image_paths[k])
        draw_pcolor(data, k, path, save=True)
        pyplot.close('all')


def diff(zone):
    """  Compute the mean image for a zone and for not-zone. Plot difference and save.
    """
    labels = load_labels()
    plus_keys = [l for l in labels if(labels[l][zone] == 1)]
    neg_keys = [l for l in labels if(labels[l][zone]) == 0]
    plus_mean, plus_std = pixel_wise_mean_and_std(plus_keys)
    neg_mean, neg_std = pixel_wise_mean_and_std(neg_keys)
    diff_mean = plus_mean - neg_mean
    diff_std = plus_std - neg_std
    draw_pcolor(
        diff_mean,
        '{0!s}_mean_diff'.format(zone),
        'clean_plots/diffs/zone_{0!s}'.format(zone)
    )
    pyplot.close('all')
    draw_pcolor(
        diff_std,
        '{0!s}_std_diff'.format(zone),
        'clean_plots/diffs/zone_{0!s}'.format(zone)
    )
    pyplot.close('all')


def crop_images(ims, zone, scale=4):
    """ crop image stacks according to crops defined by zone_params.py
    """
    points = zone_params.zone_crop_list[zone]
    masks = zone_params.zone_slice_list[zone]
    ret = []
    for i in range(len(points)):
        if(points[i] is not None and masks[i] is not None):
            # temp = np.zeros_like(ims[:, :, :, :1])
            # y, x, yw, xw = [int(e / scale) for e in masks[i]]
            # temp[:, x:xw, y:yw, 0:1] = ims[:, x:xw, y:yw, i:i + 1]
            y, x, yw, xw = [int(e / scale) for e in points[i]]
            # ret.append(temp[:, y:y + yw, x:x + xw, :])
            ret.append(ims[:, y:y + yw, x:x + xw, i:i + 1])
    return np.array(ret).transpose([1, 2, 3, 0, 4])


def show_cropped_means(zone):
    x = np.load('mean_image_array.npy')
    x = x.transpose([1, 0, 2])
    x = np.flipud(x)
    ret = crop_images(np.array([x]), zone, scale=1)[0, :, :, :, 0]
    points = zone_params.zone_crop_list[zone]
    print(ret.shape)
    count = 0
    for i in range(len(points)):
        if(points[i] is not None):
            f, axarr = pyplot.subplots(1, 2)
            axarr[0].set_title('full_{0!s}'.format(i))
            axarr[0].imshow(x[:, :, i])
            axarr[1].set_title('crop_{0!s}'.format(i))
            axarr[1].imshow(ret[:, :, count])
            pyplot.show()
            count += 1


if(__name__ == '__main__'):
    p = '0043db5e8c819bffc15261b1f1ac5e42.aps'
    show_cropped_means(16)
    """
    # parse log files and report best values
    for zone in range(17):
        csv_path_template = '../models/zone_{0!s}/log_zone_{0!s}.csv'
        csv = csv_path_template.format(zone)
        if(os.path.exists(csv)):
            print(zone)
            with open(csv) as temp_f:
                log_data = temp_f.read().split()[1:]
            log_data = [l.split(',') for l in log_data]
            best_entry = min(log_data, key=lambda x: float(x[-1]))
            print(best_entry)
    """
